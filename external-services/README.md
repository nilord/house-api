# External API Documentacion

## Correr con docker

La manera más sencilla de levantar este servicio es mediante docker. Para hacer esto deberás compilar el source y a continuación mediante el archivo .yml realizar la respectiva creación del contenedor:

```console
./mvnw package && docker-compose up -d
```

En caso de no querer compilar el source puedes descargar la última versión del servicio mediante el registry de docker hub:

```console
docker run -d -p 9090:9090 nilord/external-services
```

## Variables de entorno
Para la ejecución exitosa de estos contenedores hay ciertas variables de entorno que son utilizada por el archivo yml para la ejecución exitosa de estas funcionalidades. Esta descripción te ayudará saber qué significa cada una:

```console
DB_API_HOME= Dirección física donde se almacenara la información de la base de datos NO SQL. Ejemplo: '/home/usuario/datainfo'
DB_API_NAME= Nombre de la base de datos donde se almacenará la información del api. Ejemplo: 'database_name'
DB_API_USER= Nombre de usuario de conexión a la base de datos. Ejemplo: 'dbUserName'
DB_API_PASSWORD= Password de conexión a la base de datos. Ejemplo: 'password123'
TELEGRAM_API_USER= Nombre de usuario de tu bot de Telegram. Ejemplo: '@NiLordBot'
TELEGRAM_API_TOKEN= Token secreto suministrado por la API de telegram. Ejemplo: '235451:AAG-O9fassASfdf-Adgasrta'
CLOUDFLARE_API_USER= Correo electrónico de tu cuenta de Cloudflare. Ejemplo: 'correo@proveedor.com'
CLOUDFLARE_API_TOKEN= Token secretro suministrado por la API de Cloudflare. Ejemplo 'TuwktztQB5vRFMhYvwnDD6Cu9tPPTWZz'
```

Estas variables de entorno pueden ser remplazadas directamente en el archivo .yml para evitar ser creada en un entorno no productivo.

## Edición del código
Para la edición de código utiliza el archivo pom.xml para importarlo a tu editor de código favorito. Siempre que tengas declarada la variable $JAVA_HOME en el Path de tu equipo puedes realizar la compilación del mismo mediante maven y spring boot:

```console
./mvnw spring-boot:run
```
o con tu instancia local de mvn (siempre que esté disponible en el PATH de tu máquina):

```console
mvn spring-boot:run
```

Dependencias como el JDK utilizado puede ser encontrado en el POM.

## Visualización del Swagger
Puedes visualizar las operaciones expuestas en el API mediante la url. Ten presente que por defecto el puerto utilizado para exponer el servicio es el 9090. Puedes modificar este puerto en el archivo yml que expone el docker por si lo estás utilizando en tu equipo.

```console
http://localhost:9090/external-services/swagger-ui.html
```