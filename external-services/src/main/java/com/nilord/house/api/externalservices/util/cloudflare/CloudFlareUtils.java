package com.nilord.house.api.externalservices.util.cloudflare;

import java.util.List;

import com.nilord.house.api.externalservices.error.NiLordServiceException;

import eu.roboflax.cloudflare.objects.dns.DNSRecord;
import eu.roboflax.cloudflare.objects.zone.Zone;

public interface CloudFlareUtils {

	/**
	 * Update DNS record for a SITE in cloudflare
	 * 
	 * @param newIPAddress to configure
	 * @throws NiLordServiceException
	 */
	public Boolean updateDNSRecord(String newIPAddress) throws NiLordServiceException;
	
	/**
	 * Get the zone for the specific domain
	 * 
	 * @return the domains zone
	 * @throws NiLordServiceException if something goes wrong
	 */
	public Zone getZoneDomain(final String site) throws NiLordServiceException;
	
	/**
	 * Return all DNS records in a specific zone
	 * 
	 * @param zoneID to search
	 * @return the list of all DNS records
	 * @throws NiLordServiceException if something when wrong
	 */
	public List<DNSRecord> getSiteDNSRecords(final String zoneID) throws NiLordServiceException;
	
}
