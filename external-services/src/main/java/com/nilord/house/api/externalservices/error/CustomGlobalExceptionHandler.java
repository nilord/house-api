package com.nilord.house.api.externalservices.error;

import java.util.Arrays;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.nilord.house.api.externalservices.constants.ExecutionState;
import com.nilord.house.api.externalservices.model.domain.NiLordServiceResponseEntity;
import com.nilord.house.api.externalservices.model.domain.ServiceHeaderDTO;

/**
 * Custom global exception Handler for services
 * 
 * @author nilord
 *
 */
@RestControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(NiLordServiceException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public NiLordServiceResponseEntity<Object> handleCustomException(NiLordServiceException ce) {
		final NiLordServiceResponseEntity<Object> response = new NiLordServiceResponseEntity<Object>();
		final ServiceHeaderDTO header = new ServiceHeaderDTO(ExecutionState.FAIL);
		
		header.setAlerts(Arrays.asList(ce.getMessage()));
		response.setHeader(header);
		
		return response;
	}
}
