package com.nilord.house.api.externalservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nilord.house.api.externalservices.command.CheckIpAddressCommand;
import com.nilord.house.api.externalservices.command.UpdateIpAddressCommand;
import com.nilord.house.api.externalservices.model.domain.NiLordServiceResponseEntity;
import com.nilord.house.api.externalservices.model.services.network.CheckIpResponseDTO;
import com.nilord.house.api.externalservices.model.services.network.UpdateIpResponseDTO;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("${api.basepath}/network")
@Api(tags = "Network operation controller")
public class NetworkController {

	@Autowired
	private CheckIpAddressCommand checkIpAddressCommand;
	
	@Autowired
	private UpdateIpAddressCommand updateIpAddressCommand;

	/**
	 * Request to see current server ip address
	 * 
	 * @return the ip address information
	 */
	@RequestMapping(value = "/ip", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = MediaType.ALL_VALUE)
	public NiLordServiceResponseEntity<CheckIpResponseDTO> searchIp() {

		return checkIpAddressCommand.findIpAddressInfo();
	}
	
	/**
	 * Upgrade external ip address if necesary
	 * 
	 * @return
	 */
	@RequestMapping(value = "/update-ip", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = MediaType.ALL_VALUE)
	public NiLordServiceResponseEntity<UpdateIpResponseDTO> updateIp() {

		return updateIpAddressCommand.updateIpAddressInfo();
	}

}
