package com.nilord.house.api.externalservices.model.domain.data;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Basic service configuration model")
@Document
public class BasicServiceConfigurationDTO implements Serializable{
	
	private static final long serialVersionUID = 3961287442466935650L;

	@Id
	private String id;
	
	@Field
	private String value;
	
	@Field
	private Boolean applied;

}
