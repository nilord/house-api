package com.nilord.house.api.externalservices.repos;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.nilord.house.api.externalservices.model.domain.data.TelegramUserDTO;

/**
 * Repository for telegramUsersDTO
 * 
 * @author nilord
 *
 */
@Repository
public interface TelegramUsersRepository extends MongoRepository<TelegramUserDTO, String> {

}
