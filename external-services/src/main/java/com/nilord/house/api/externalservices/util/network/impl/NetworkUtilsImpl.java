package com.nilord.house.api.externalservices.util.network.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nilord.house.api.externalservices.error.NiLordServiceException;
import com.nilord.house.api.externalservices.util.network.NetworkUtils;

import lombok.extern.slf4j.Slf4j;

@Component("NetworkUtils")
@Slf4j
public class NetworkUtilsImpl implements NetworkUtils {

	@Value("${network.ip.url}")
	private String AWS_URL;

	@Override
	public String getPrivateIPAddress() throws NiLordServiceException {
		String response = null;

		try {
			response = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			log.error("Ocurrio un error determinando IP LOCAL:", e);
			throw new NiLordServiceException(e.getMessage());
		}

		return response;
	}

	@Override
	public String getPublicIPAddress() throws NiLordServiceException {
		String response = null;
		URL whatismyip;
		try {
			whatismyip = new URL(AWS_URL);
			BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
			response = in.readLine();
		} catch (MalformedURLException e) {
			log.error("MalformedURLException: ", e);
			throw new NiLordServiceException("Ocurrio un error con la URL remota");
		} catch (IOException e) {
			log.error("IOException: ", e);
			throw new NiLordServiceException("La URL no devolvio ninguna informacion");
		}

		return response;
	}

}
