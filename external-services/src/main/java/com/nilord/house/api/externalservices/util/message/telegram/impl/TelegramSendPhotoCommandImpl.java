package com.nilord.house.api.externalservices.util.message.telegram.impl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nilord.house.api.externalservices.error.NiLordServiceException;
import com.nilord.house.api.externalservices.util.message.telegram.TelegramSendPhotoCommand;

import lombok.extern.slf4j.Slf4j;

@Component("TelegramSendPhotoCommand")
@Slf4j
public class TelegramSendPhotoCommandImpl implements TelegramSendPhotoCommand {

	@Value("${message.telegram.bot.cat.url}")
	private String CAT_URL;

	@Override
	public File sendTestPhoto() throws NiLordServiceException {

		File outputfile = new File("/tmp/image.jpg");

		try {
			final URL url = new URL(CAT_URL);
			BufferedImage image = ImageIO.read(url);
			ImageIO.write(image, "jpg", outputfile);
		} catch (IOException e) {
			log.error("Ocurrio un problema buscando la imagen {}", e);
			throw new NiLordServiceException("Ocurrio un problema buscando la imagen");
		}

		return outputfile;
	}

}
