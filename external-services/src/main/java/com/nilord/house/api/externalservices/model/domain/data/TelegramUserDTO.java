package com.nilord.house.api.externalservices.model.domain.data;

import java.io.Serializable;
import java.text.MessageFormat;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.nilord.house.api.externalservices.constants.UserStatusEnum;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Telegram user information to save approval")
@Document
public class TelegramUserDTO implements Serializable {
	
	private static final long serialVersionUID = -3567328709732436243L;

	@Id
	@Field
	private Integer id; ///< Unique identifier for this user or bot

	@Field
    private String firstName; ///< Users or bots first name

	@Field
    private Boolean isBot; ///< True, if this user is a bot

	@Field
    private String lastName; ///< Optional. Users or Bots last name

	@Field
    private String userName; ///< Optional. Users or Bots username

	@Field
    private String languageCode; ///< Optional. IETF language tag of the user's language

	@Field
    private Boolean canJoinGroups; ///< Optional. True, if the bot can be invited to groups. Returned only in getMe.

	@Field
    private Boolean canReadAllGroupMessages; ///< Optional. True, if privacy mode is disabled for the bot. Returned only in getMe.

	@Field
    private Boolean supportInlineQueries; ///< Optional. True, if the bot supports inline queries. Returned only in getMe.
    
	@Field
    private Integer userRole; // Own atribute (Do I Know this Guy?)
	
	@Override
	public String toString() {
		final String name = userName == null ? firstName : userName;
		return MessageFormat.format("{0} <{1}> : {2}<{3}>", name, id, UserStatusEnum.getEnumFromId(userRole).getName(),
				userRole);
	}
}
