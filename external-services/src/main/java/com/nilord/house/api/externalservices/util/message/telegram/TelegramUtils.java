package com.nilord.house.api.externalservices.util.message.telegram;

import com.nilord.house.api.externalservices.constants.UserStatusEnum;
import com.nilord.house.api.externalservices.error.NiLordServiceException;

/**
 * Utilities for Telegram
 * 
 * @author nilord
 *
 */
public interface TelegramUtils {

	/**
	 * Send masive telegram alert too all user in database with specific role
	 * 
	 * @param message text message to send
	 * @param role role to send the message
	 * @throws NiLordServiceException
	 */
	public void sendTextMessagesPerRole(String message, UserStatusEnum role) throws NiLordServiceException;

}
