package com.nilord.house.api.externalservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ExternalServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExternalServicesApplication.class, args);
	}

}
