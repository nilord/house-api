package com.nilord.house.api.externalservices.util.message.telegram;

import java.io.File;

import com.nilord.house.api.externalservices.error.NiLordServiceException;

public interface TelegramSendPhotoCommand {
	
	/**
	 * Send a cat test photo
	 * 
	 * @return
	 * @throws NiLordServiceException
	 */
	public File sendTestPhoto() throws NiLordServiceException;

}
