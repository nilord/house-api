package com.nilord.house.api.externalservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nilord.house.api.externalservices.command.SendTelegramRoleNotificationCommand;
import com.nilord.house.api.externalservices.model.domain.NiLordServiceResponseEntity;
import com.nilord.house.api.externalservices.model.services.notification.BasicNotificationDTO;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("${api.basepath}/notification")
@Api(tags = "Notification controller")
public class NotificationController {
	
	@Autowired
	private SendTelegramRoleNotificationCommand sendTelegramRoleNotificationCommand;
	
	/**
	 * Send a telegram notification with the text message to a specific role
	 * 
	 * @param request
	 * @return true - successful, false - Failed
	 */
	@RequestMapping(value = "/telegram/sendRoleNotification", method = RequestMethod.POST, produces =
			MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public NiLordServiceResponseEntity<Boolean> sendTelegramRoleNotification( @RequestBody BasicNotificationDTO request) {

		return sendTelegramRoleNotificationCommand.sendNotificationPerRole(request);
	}

}
