package com.nilord.house.api.externalservices.command;

import com.nilord.house.api.externalservices.error.NiLordServiceException;
import com.nilord.house.api.externalservices.model.domain.NiLordServiceResponseEntity;
import com.nilord.house.api.externalservices.model.services.notification.BasicNotificationDTO;

/**
 * SendTelegramRoleNotification interface 
 * 
 * @author nilord
 *
 */
public interface SendTelegramRoleNotificationCommand {

	/**
	 * Send notification using telegram to a specific group of users
	 * 
	 * @return
	 * @throws NiLordServiceException
	 */
	public NiLordServiceResponseEntity<Boolean> sendNotificationPerRole(BasicNotificationDTO request)
			throws NiLordServiceException;

}
