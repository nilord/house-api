package com.nilord.house.api.externalservices.util.message.telegram.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.MessageEntity;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import com.google.gson.Gson;
import com.nilord.house.api.externalservices.constants.TelegramCommands;
import com.nilord.house.api.externalservices.constants.UserStatusEnum;
import com.nilord.house.api.externalservices.error.NiLordServiceException;
import com.nilord.house.api.externalservices.model.domain.TelegramCommandDTO;
import com.nilord.house.api.externalservices.model.domain.data.TelegramUserDTO;
import com.nilord.house.api.externalservices.repos.TelegramUsersRepository;
import com.nilord.house.api.externalservices.util.message.telegram.TelegramSendMessageCommand;
import com.nilord.house.api.externalservices.util.message.telegram.TelegramSendPhotoCommand;
import com.nilord.house.api.externalservices.util.message.telegram.TelegramUtils;

import lombok.extern.slf4j.Slf4j;

@Component("TelegramUtils")
@Slf4j
public class TelegramUtilsImpl extends TelegramLongPollingBot implements TelegramUtils {

	@Value("${message.telegaram.bot.username}")
	private String botUsername;

	@Value("${message.telegaram.bot.token}")
	private String secretToken;

	@Value("${message.telegram.bot.command.unknown}")
	private String UNKNOWN_COMMAND_ERROR;

	@Autowired
	private TelegramSendMessageCommand telegramSendMessageCommand;

	@Autowired
	private TelegramSendPhotoCommand telegramSendPhotoCommand;

	@Autowired
	private TelegramUsersRepository telegramUsersRepository;

	// ********************* Herency Methods *************************** //

	@Override
	public String getBotUsername() {
		return botUsername;
	}

	@Override
	public String getBotToken() {
		return secretToken;
	}

	@Override
	public void onUpdateReceived(Update update) {
		if (update.hasMessage()) {
			final Message message = update.getMessage();

			final List<TelegramCommandDTO> commands = this.getCommandList(message);

			for (TelegramCommandDTO command : commands) {
				switch (TelegramCommands.getType(command.getId())) {
				case SendMessage:
					SendMessage smResponse = processMessageCommand(command);
					send(smResponse);
					break;
				case SendPhoto:
					SendPhoto spResponse = processPhotoCommand(command);
					send(spResponse);
					break;
				default:
					smResponse = processMessageCommand(command);
					send(smResponse);
				}
			}
		}
	}

	// ********************* Implemented Methods *************************** //

	@Override
	@Async
	public void sendTextMessagesPerRole(String message, UserStatusEnum role) throws NiLordServiceException {
		final Iterable<TelegramUserDTO> users = telegramUsersRepository.findAll();

		for (TelegramUserDTO user : users) {
			if (role.getId().equals(user.getUserRole())) {
				final SendMessage response = new SendMessage();
				response.setText(message);
				response.setChatId(user.getId().toString());
				this.send(response);
			}
		}
	}

	// ********************* Private Methods *************************** //

	/**
	 * Process command message
	 * 
	 * @param message
	 * @return
	 */
	private SendMessage processMessageCommand(TelegramCommandDTO command) {
		String message = UNKNOWN_COMMAND_ERROR;

		final SendMessage response = new SendMessage();
		response.setChatId(String.valueOf(command.getSource().getChatId()));

		switch (TelegramCommands.getEnumFromId(command.getId())) {
		case CHECK_IP:
			message = telegramSendMessageCommand.checkIPCommand(command);
			break;
		case START:
			message = telegramSendMessageCommand.startCommand(command);
			break;
		case HELP:
			message = telegramSendMessageCommand.getHelpCommand(command);
			break;
		case UPDATE_IP:
			message = telegramSendMessageCommand.updateIPCommand(command);
			break;
		case LIST_USERS:
			message = telegramSendMessageCommand.getUsers(command);
			break;
		case UPDATE_USER:
			message = telegramSendMessageCommand.updateUserRole(command);
			break;
		default:
			message = UNKNOWN_COMMAND_ERROR;
			break;
		}

		response.setText(message);

		return response;
	}

	private SendPhoto processPhotoCommand(TelegramCommandDTO command) {

		final SendPhoto response = new SendPhoto();
		response.setChatId(String.valueOf(command.getSource().getChatId()));
		response.setPhoto(new InputFile());

		switch (TelegramCommands.getEnumFromId(command.getId())) {
		case PHOTO:
			response.getPhoto().setMedia(telegramSendPhotoCommand.sendTestPhoto());
			break;
		default:
			break;
		}

		return response;
	}

	/**
	 * Send specific message using telegram SendMesage
	 * 
	 * @param message text + chatId parameters
	 * 
	 * @throws NiLordServiceException
	 */
	private void send(SendMessage message) throws NiLordServiceException {
		try {
			execute(message);
			log.info("Sent message \"{}\" to {}", message.getText(), message.getChatId());
		} catch (TelegramApiException e) {
			log.error("Failed to send message \"{}\" to {} due to error: {}", message.getText(), message.getChatId(),
					e.getMessage());
			throw new NiLordServiceException("Error enviado mensaje de telegram: " + message.getText());
		}
	}

	/**
	 * Send message phototype
	 * 
	 * @param photo
	 * @throws NiLordServiceException
	 */
	private void send(SendPhoto photo) throws NiLordServiceException {
		try {
			execute(photo);
		} catch (TelegramApiException e) {
			log.error("Failed to send message photo to {} due to error: {}", photo.getChatId(), e);
			throw new NiLordServiceException("Error enviado mensaje de telegram: " + e.getMessage());
		}
	}

	/**
	 * Returns list of all commands in a message
	 * 
	 * @param message
	 * @return
	 */
	private List<TelegramCommandDTO> getCommandList(Message message) {
		final List<TelegramCommandDTO> response = new ArrayList<TelegramCommandDTO>();
		String messageText = message.getText().trim();

		if (null == message.getEntities() || message.getEntities().isEmpty()) {
			return response;
		}

		for (MessageEntity entity : message.getEntities()) {
			if ("bot_command".equals(entity.getType())) {
				TelegramCommandDTO command = new TelegramCommandDTO();
				command.setParameters(new ArrayList<String>());
				Integer commandFinishPos = entity.getOffset() + entity.getLength();

				command.setId(message.getText().substring(entity.getOffset(), commandFinishPos));

				messageText = message.getText().substring(commandFinishPos).trim();

				// If next position is not a command and the string value is more than 0, I'll
				// try to find commands parameters
				if (messageText.indexOf("/") != 0 && messageText.length() != 0) {
					messageText = messageText.substring(0,
							(messageText.indexOf("/") == -1 ? messageText.length() + 1 : messageText.indexOf("/")) - 1);
					command.setParameters(Arrays.asList(messageText.split(" ")));
				}

				command.setSource(message);

				response.add(command);
			}
		}

		log.info("Recibido comandos a procesar {}", new Gson().toJson(response));

		return response;
	}
}
