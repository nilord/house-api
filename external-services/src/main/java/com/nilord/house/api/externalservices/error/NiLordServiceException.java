package com.nilord.house.api.externalservices.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Main exception class for services
 * 
 * @author nilord
 *
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Ocurrio un error inesperado")
public class NiLordServiceException extends RuntimeException {

	private static final long serialVersionUID = 2674880805289764721L;

	public NiLordServiceException(String errorMessage) {
		super(errorMessage);
	}

}
