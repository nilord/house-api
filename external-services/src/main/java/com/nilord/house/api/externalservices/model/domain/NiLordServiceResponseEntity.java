package com.nilord.house.api.externalservices.model.domain;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Base service entity for all nilord responses.
 * 
 * @author nilord
 *
 * @param <T> class to implement
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Common service response entity")
public class NiLordServiceResponseEntity <T> implements Serializable{

	private static final long serialVersionUID = -5173628384994291687L;
	
	@ApiModelProperty(notes = "Service header", position= 0, required = true)
	private ServiceHeaderDTO header;
	
	@ApiModelProperty(notes = "Response body", position= 1, required = false)
	private T body;
	
}
