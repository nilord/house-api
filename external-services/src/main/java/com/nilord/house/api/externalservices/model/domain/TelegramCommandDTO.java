package com.nilord.house.api.externalservices.model.domain;

import java.io.Serializable;
import java.util.List;

import org.telegram.telegrambots.meta.api.objects.Message;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Represents a telegram command with its respective parameter")
public class TelegramCommandDTO implements Serializable {
	
	private static final long serialVersionUID = 2064105826407195244L;
	
	@ApiModelProperty(notes = "Command id", position= 0, example = "/start", required = false)
	private String id;
	
	@ApiModelProperty(notes = "Command parameters", position= 1, required = false)
	private List<String> parameters;
	
	@ApiModelProperty(notes = "Message source", position= 1, required = false)
	private Message source;

}
