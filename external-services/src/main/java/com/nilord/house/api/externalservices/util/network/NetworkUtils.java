package com.nilord.house.api.externalservices.util.network;

import com.nilord.house.api.externalservices.error.NiLordServiceException;

/**
 * Network Utils Interface
 *
 * @author nilord
 *
 */
public interface NetworkUtils {
	
	/**
	 * Get host private IP address 
	 * @return IP address string
	 * @throws NiLordServiceException is something fails
	 */
	public String getPrivateIPAddress() throws NiLordServiceException;
	
	/**
	 * Get host public IP address 
	 * @return IP address string
	 * @throws NiLordServiceException is something fails
	 */
	public String getPublicIPAddress() throws NiLordServiceException;

}
