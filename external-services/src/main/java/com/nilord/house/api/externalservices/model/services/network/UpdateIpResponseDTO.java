package com.nilord.house.api.externalservices.model.services.network;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Update IP Address response DTO
 * 
 * @author nilord
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Response entity for update ip address command")
@ToString
public class UpdateIpResponseDTO {
	
	@ApiModelProperty(notes = "Last external Ip Address know", position= 0, example = "190.219.206.7", required = false)
	private String oldExternalIp;
	
	@ApiModelProperty(notes = "New external IP Address at this moment", position= 1, example = "190.219.206.7", required = false)
	private String newExternalIp;
	
	@ApiModelProperty(notes = "Old external IP has changed", position= 3, example = "false", required = false)
	private Boolean hasChange;
	
	@ApiModelProperty(notes = "Is the configuration applied", position= 4, example = "false", required = false)
	private Boolean applied;
	
}
