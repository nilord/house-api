package com.nilord.house.api.externalservices.constants;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import lombok.Getter;

@Getter
public enum TelegramCommands {
	UNKNOWN("COMANDO NO CONOCIDO", "Comando no reconocido", 0, Arrays.asList(), CommandResponseType.SendMessage),
	START("/start", "Comando que valida que tengas acceso a ejecutar comandos dentro del bot", 1, Arrays.asList(), CommandResponseType.SendMessage),
	CHECK_IP("/check_ip", "Comando que muestra la informacion del IP del equipo", 1, Arrays.asList(), CommandResponseType.SendMessage),
	UPDATE_IP("/update_ip", "Comando que actualiza la informacion del IP publico en cloudflare", 100, Arrays.asList(), CommandResponseType.SendMessage),
	LIST_USERS("/list_users", "Comando que muestra una lista de todos los usuarios del bot", 100, Arrays.asList(), CommandResponseType.SendMessage),
	UPDATE_USER("/update_user", "Comando que actualiza el rol de un usuario.", 100,
			Arrays.asList("{id_usuario}", "{id_rol}"), CommandResponseType.SendMessage),
	HELP("/help", "Comando que muestra la ayuda", 1, Arrays.asList(), CommandResponseType.SendMessage),
	PHOTO("/photo", "Envia una imagen de muestra", 1, Arrays.asList(), CommandResponseType.SendPhoto);

	private String id;
	private String description;
	private Integer requiredLevel;
	private List<String> requiredParams;
	private CommandResponseType type;

	TelegramCommands(String id, String description, Integer level, List<String> requiredParams, CommandResponseType type) {
		this.id = id;
		this.description = description;
		this.requiredLevel = level;
		this.requiredParams = requiredParams;
		this.type = type;
	}

	/**
	 * Return enum from id
	 * 
	 * @param id
	 * @return
	 */
	public static TelegramCommands getEnumFromId(String id) {
		TelegramCommands response = TelegramCommands.UNKNOWN;

		for (TelegramCommands command : TelegramCommands.values()) {
			if (command.getId().equals(id.toLowerCase())) {
				return command;
			}
		}

		return response;
	}

	/**
	 * Get example of line execution
	 * 
	 * @param id
	 * @return
	 */
	public static String getExample(String id) {
		return getExample(getEnumFromId(id));
	}

	/**
	 * Get example of line execution
	 * 
	 * @param enum
	 * @return
	 */
	public static String getExample(TelegramCommands commandEnum) {
		String response = commandEnum.getId();

		final String params = String.join(StringUtils.SPACE, commandEnum.requiredParams);

		response = response.concat(StringUtils.SPACE).concat(params);

		return response;
	}
	
	/**
	 * Get command type
	 * 
	 * @param id
	 * @return
	 */
	public static CommandResponseType getType(String id) {
		return getEnumFromId(id).getType();
	}

	@Override
	public String toString() {
		return id + " - " + description;
	}

}
