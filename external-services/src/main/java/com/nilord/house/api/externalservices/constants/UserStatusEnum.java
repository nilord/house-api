package com.nilord.house.api.externalservices.constants;

import lombok.Getter;

@Getter
public enum UserStatusEnum {
	INVALID(-1, "INVALID ROLE"),
	UNKNOWN(0, "UNKNOWN USER"),
	REGULAR(1, "REGULAR USER"),
	ADMIN(100, "ADMIN USER");

	private Integer id;
	private String name;
	
	public static UserStatusEnum getEnumFromId(Integer id){
		UserStatusEnum response = UserStatusEnum.INVALID;
		
		for (UserStatusEnum command : UserStatusEnum.values()) { 
		    if(command.getId() == id) {
		    	return command;
		    }
		}
		
		return response;
	}
	
	UserStatusEnum(Integer id, String name) {
		this.id = id;
		this.name = name;
	}
	
}
