package com.nilord.house.api.externalservices.command;

import com.nilord.house.api.externalservices.error.NiLordServiceException;
import com.nilord.house.api.externalservices.model.domain.NiLordServiceResponseEntity;
import com.nilord.house.api.externalservices.model.services.network.UpdateIpResponseDTO;

/**
 * The Interface UpdateIpAddressCommand
 * 
 * @author nilord
 *
 */
public interface UpdateIpAddressCommand {

	/**
	 * Update the ip adress info (external)
	 * 
	 * @return the ip adress information
	 */
	public NiLordServiceResponseEntity<UpdateIpResponseDTO> updateIpAddressInfo() throws NiLordServiceException;
	
}
