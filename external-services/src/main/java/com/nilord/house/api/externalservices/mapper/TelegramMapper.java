package com.nilord.house.api.externalservices.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.telegram.telegrambots.meta.api.objects.User;

import com.nilord.house.api.externalservices.model.domain.data.TelegramUserDTO;

@Mapper(componentModel = "spring")
public interface TelegramMapper {
	
	public TelegramMapper INSTANCE = Mappers.getMapper( TelegramMapper.class );
	
	/**
	 * Transform User to API Telegram User entity
	 * 
	 * @param input telegram default user
	 * 
	 * @return 
	 */
	@Mapping(target = "userRole", constant  = "-1")
	TelegramUserDTO userToTelegramUserDTO(User input);
	
	
	/**
	 * Transform API Telegram User entity to user
	 * 
	 * @param input telegram default user
	 * 
	 * @return 
	 */
	User telegramUserDTOToUser(TelegramUserDTO input);

}
