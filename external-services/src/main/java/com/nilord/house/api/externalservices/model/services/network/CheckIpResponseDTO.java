package com.nilord.house.api.externalservices.model.services.network;

import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Check IP Adress response DTO
 * 
 * @author nilord
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Response entity for find ip address command")
public class CheckIpResponseDTO implements Serializable{

	private static final long serialVersionUID = 7777744968553540002L;
	
	@ApiModelProperty(notes = "External ip address", position= 0, example = "190.219.206.7", required = false)
	private String externalIp;
	
	@ApiModelProperty(notes = "Internal ip address", position= 1, example = "192.168.10.54", required = false)
	private String internalIp;
	
	@ApiModelProperty(notes = "Query date time", position= 3, example = "2019-08-19T15:05:59.785Z", required = false)
	private LocalDateTime queryDateTime;
}
