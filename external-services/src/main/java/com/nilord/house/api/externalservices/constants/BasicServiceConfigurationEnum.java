package com.nilord.house.api.externalservices.constants;

import lombok.Getter;

@Getter
public enum BasicServiceConfigurationEnum {
	LAST_IP_KNOW("1");
	
	private String id;
	
	BasicServiceConfigurationEnum(String id) {
		this.id = id;
	}

}
