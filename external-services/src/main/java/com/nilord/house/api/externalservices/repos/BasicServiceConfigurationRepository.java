package com.nilord.house.api.externalservices.repos;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.nilord.house.api.externalservices.model.domain.data.BasicServiceConfigurationDTO;

/**
 * Interface BasicServiceConfigurationRepository
 * 
 * @author nilord
 *
 */
@Repository
public interface BasicServiceConfigurationRepository extends MongoRepository<BasicServiceConfigurationDTO, String> {

}
