package com.nilord.house.api.externalservices.util.message.telegram;

import com.nilord.house.api.externalservices.error.NiLordServiceException;
import com.nilord.house.api.externalservices.model.domain.TelegramCommandDTO;

public interface TelegramSendMessageCommand {

	/**
	 * Say hi if the users at least have a UserStatusEnum more powerfull than
	 * "Unknown"
	 * 
	 * @return
	 * @throws NiLordServiceException
	 */
	public String startCommand(TelegramCommandDTO command) throws NiLordServiceException;

	/**
	 * Execute check ip telegram command
	 * 
	 * @return
	 * @throws NiLordServiceException
	 */
	public String checkIPCommand(TelegramCommandDTO command) throws NiLordServiceException;
	
	/**
	 * Returns available commands for the user level
	 * 
	 * @return
	 * @throws NiLordServiceException
	 */
	public String getHelpCommand(TelegramCommandDTO command) throws NiLordServiceException;
	
	/**
	 * Update server ip adress in cloudflare (Admin only)
	 * 
	 * @return
	 * @throws NiLordServiceException
	 */
	public String updateIPCommand(TelegramCommandDTO command) throws NiLordServiceException;
	
	/**
	 * Get all users in the database
	 * 
	 * @param message
	 * @return
	 * @throws NiLordServiceException
	 */
	public String getUsers(TelegramCommandDTO command) throws NiLordServiceException;
	
	/**
	 * Update selected users role
	 * 
	 * @param message
	 * @return
	 * @throws NiLordServiceException
	 */
	public String updateUserRole(TelegramCommandDTO command) throws NiLordServiceException;

}
