package com.nilord.house.api.externalservices.model.services.notification;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Request entity for send telegram notification")
public class BasicNotificationDTO implements Serializable {
	
	private static final long serialVersionUID = 9210797628202390062L;
	
	@ApiModelProperty(notes = "Text message", position= 0, example = "Hola, esta es una notificacion", required = true)
	private String message;
	
	@ApiModelProperty(notes = "Role id for the notification", position= 1, example = "1", required = true)
	private Integer roleId;

}
