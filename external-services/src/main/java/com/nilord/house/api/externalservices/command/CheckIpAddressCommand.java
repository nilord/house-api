package com.nilord.house.api.externalservices.command;

import com.nilord.house.api.externalservices.error.NiLordServiceException;
import com.nilord.house.api.externalservices.model.domain.NiLordServiceResponseEntity;
import com.nilord.house.api.externalservices.model.services.network.CheckIpResponseDTO;

/**
 * Command to find the IP address
 * 
 * @author nilord
 *
 */
public interface CheckIpAddressCommand {

	/**
	 * Find the ip adress info (external and internal)
	 * 
	 * @return the ip adress information
	 */
	public NiLordServiceResponseEntity<CheckIpResponseDTO> findIpAddressInfo() throws NiLordServiceException;

}
