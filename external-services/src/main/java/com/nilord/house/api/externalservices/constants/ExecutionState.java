package com.nilord.house.api.externalservices.constants;

import com.nilord.house.api.externalservices.model.domain.HeaderStatusDTO;

import lombok.Getter;

@Getter
public enum ExecutionState {
	SUCESS("U0000", "Ejecucion exitosa"), 
	WARNING("U0001", "Ejecución con posibles alertas"),
	FAIL("U0002", "Ejecucion fallida");

	private String id;
	private String name;

	ExecutionState(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public static HeaderStatusDTO getValueFromState(ExecutionState status) {
		return new HeaderStatusDTO(status.getId(), status.getName());
	}
}
