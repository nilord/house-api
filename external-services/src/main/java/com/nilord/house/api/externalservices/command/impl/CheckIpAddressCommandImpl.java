package com.nilord.house.api.externalservices.command.impl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nilord.house.api.externalservices.command.CheckIpAddressCommand;
import com.nilord.house.api.externalservices.constants.ExecutionState;
import com.nilord.house.api.externalservices.error.NiLordServiceException;
import com.nilord.house.api.externalservices.model.domain.NiLordServiceResponseEntity;
import com.nilord.house.api.externalservices.model.domain.ServiceHeaderDTO;
import com.nilord.house.api.externalservices.model.services.network.CheckIpResponseDTO;
import com.nilord.house.api.externalservices.util.network.NetworkUtils;

import lombok.extern.slf4j.Slf4j;

@Component("CheckIpAddressCommand")
@Slf4j
public class CheckIpAddressCommandImpl implements CheckIpAddressCommand {

	@Autowired
	private NetworkUtils networkUtils;

	@Override
	public NiLordServiceResponseEntity<CheckIpResponseDTO> findIpAddressInfo() throws NiLordServiceException {
		log.info("Inicializando comando para busqueda de direccion IP");

		final NiLordServiceResponseEntity<CheckIpResponseDTO> response = new NiLordServiceResponseEntity<CheckIpResponseDTO>();

		response.setBody(new CheckIpResponseDTO(networkUtils.getPublicIPAddress(), networkUtils.getPrivateIPAddress(),
				LocalDateTime.now()));
		response.setHeader(new ServiceHeaderDTO(ExecutionState.SUCESS));

		log.info("Finalizando comando para busqueda de direccion IP");

		return response;
	}
}
