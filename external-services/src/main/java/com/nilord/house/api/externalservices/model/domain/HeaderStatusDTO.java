package com.nilord.house.api.externalservices.model.domain;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Common header status entity")
public class HeaderStatusDTO implements Serializable {
	
	private static final long serialVersionUID = -3374982880742393941L;
	
	@ApiModelProperty(notes = "Status ID", required = true, example = "U0000")
	private String id;
	
	@ApiModelProperty(notes = "Status description", required = false, example = "SUCCESS")
	private String description;

}
