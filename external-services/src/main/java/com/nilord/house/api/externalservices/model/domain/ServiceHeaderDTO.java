package com.nilord.house.api.externalservices.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.nilord.house.api.externalservices.constants.ExecutionState;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Common service header entity")
public class ServiceHeaderDTO implements Serializable {

	private static final long serialVersionUID = -8581307762393474700L;

	@ApiModelProperty(notes = "Status information", position = 0, required = true)
	private HeaderStatusDTO status;
	
	@ApiModelProperty(notes = "Alerts information", position = 1, required = false)
	private List<String> alerts;

	/**
	 * Only set status execution
	 * 
	 * @param status
	 */
	public ServiceHeaderDTO(ExecutionState status) {
		this.setStatus(ExecutionState.getValueFromState(status));
		this.setAlerts(new ArrayList<String>());
	}
}
