package com.nilord.house.api.externalservices.util.message.telegram.impl;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.User;

import com.nilord.house.api.externalservices.command.CheckIpAddressCommand;
import com.nilord.house.api.externalservices.command.UpdateIpAddressCommand;
import com.nilord.house.api.externalservices.constants.TelegramCommands;
import com.nilord.house.api.externalservices.constants.UserStatusEnum;
import com.nilord.house.api.externalservices.error.NiLordServiceException;
import com.nilord.house.api.externalservices.mapper.TelegramMapper;
import com.nilord.house.api.externalservices.model.domain.TelegramCommandDTO;
import com.nilord.house.api.externalservices.model.domain.data.TelegramUserDTO;
import com.nilord.house.api.externalservices.repos.TelegramUsersRepository;
import com.nilord.house.api.externalservices.util.message.telegram.TelegramSendMessageCommand;

import lombok.extern.slf4j.Slf4j;

@Component("TelegramSendMessageCommand")
@Slf4j
public class TelegramSendMessageCommandImpl implements TelegramSendMessageCommand {

	@Value("${message.telegram.bot.command.welcome}")
	private String WELCOME_MESSAGE;

	@Value("${message.telegram.bot.unknown}")
	private String COMMON_ERROR;

	@Value("${message.telegram.bot.command.done}")
	private String DONE_MESSAGE;

	@Value("${message.telegram.bot.command.bad}")
	private String BAD_REQUEST;

	@Autowired
	private TelegramMapper telegramMapper;

	@Autowired
	private TelegramUsersRepository telegramUsersRepository;

	@Autowired
	private CheckIpAddressCommand checkIpAddressCommand;

	@Autowired
	private UpdateIpAddressCommand updateIpAddressCommand;

	@Override
	public String startCommand(TelegramCommandDTO command) throws NiLordServiceException {

		if (!this.validUser(command)) {
			return commonAccessDeniedResponse(command.getSource().getFrom());
		}

		return WELCOME_MESSAGE;
	}

	@Override
	public String checkIPCommand(TelegramCommandDTO command) throws NiLordServiceException {

		if (!this.validUser(command)) {
			return commonAccessDeniedResponse(command.getSource().getFrom());
		}

		return checkIpAddressCommand.findIpAddressInfo().getBody().toString();
	}

	@Override
	public String getHelpCommand(TelegramCommandDTO command) throws NiLordServiceException {

		final User sourceUser = command.getSource().getFrom();

		if (!this.validUser(command)) {
			return commonAccessDeniedResponse(command.getSource().getFrom());
		}
		
		final List<String> parameters = command.getParameters();

		if (parameters.size() > 0) {
			return TelegramCommands.getExample("/" + parameters.get(0));
		}

		final TelegramUserDTO user = telegramUsersRepository.findById(String.valueOf(sourceUser.getId())).get();

		return Arrays.asList(TelegramCommands.values()).stream()
				.filter(c -> !TelegramCommands.UNKNOWN.equals(c) && c.getRequiredLevel() <= user.getUserRole())
				.sorted((c1, c2) -> c1.getRequiredLevel().compareTo(c2.getRequiredLevel())).map(Enum::toString)
				.collect(Collectors.joining(System.lineSeparator()));
	}

	@Override
	public String updateIPCommand(TelegramCommandDTO command) throws NiLordServiceException {

		if (!this.validUser(command)) {
			return commonAccessDeniedResponse(command.getSource().getFrom());
		}

		updateIpAddressCommand.updateIpAddressInfo();

		return DONE_MESSAGE;
	}

	@Override
	public String getUsers(TelegramCommandDTO command) throws NiLordServiceException {

		if (!this.validUser(command)) {
			return commonAccessDeniedResponse(command.getSource().getFrom());
		}

		final List<TelegramUserDTO> users = telegramUsersRepository.findAll();

		return users.stream().map(c -> c.toString()).collect(Collectors.joining(System.lineSeparator()));
	}

	@Override
	public String updateUserRole(TelegramCommandDTO command) throws NiLordServiceException {

		if (!this.validUser(command)) {
			return commonAccessDeniedResponse(command.getSource().getFrom());
		}

		final List<String> parameters = command.getParameters();

		if (parameters.size() < TelegramCommands.getEnumFromId(command.getId()).getRequiredParams().size()
				|| !NumberUtils.isParsable(parameters.get(1))) {
			return MessageFormat.format(BAD_REQUEST, command.getId().substring(1));
		}

		final Optional<TelegramUserDTO> user = telegramUsersRepository.findById(parameters.get(0));

		if (user.isEmpty()
				|| UserStatusEnum.INVALID.equals(UserStatusEnum.getEnumFromId(Integer.parseInt(parameters.get(1))))) {
			return "Usuario o rol enviado no existen.";
		}
		
		user.get().setUserRole(Integer.parseInt(parameters.get(1)));
		
		telegramUsersRepository.save(user.get());

		return DONE_MESSAGE;
	}

	// ********************* Private Methods *************************** //

	/**
	 * Validate if the users got the enough acess to execute a command
	 * 
	 * @param telegramUser user object from telegram
	 * @param command      command trying to be executed
	 * @return true has access, false has not access
	 */
	private Boolean validUser(TelegramCommandDTO command) {

		final User userSource = command.getSource().getFrom();
		final TelegramCommands commandEnum = TelegramCommands.getEnumFromId(command.getId());

		Optional<TelegramUserDTO> user = telegramUsersRepository.findById(String.valueOf(userSource.getId()));

		if (user.isEmpty()) {
			user = Optional.of(telegramMapper.userToTelegramUserDTO(userSource));
			telegramUsersRepository.save(user.get());
		}

		return user.get().getUserRole() >= commandEnum.getRequiredLevel();
	}

	/**
	 * Return common error for access denied command
	 * 
	 * @param user
	 * @return
	 */
	private String commonAccessDeniedResponse(User user) {
		log.warn("Usuario {} trata de ejecutar {} sin suficientes permisos", user.getId(),
				TelegramCommands.START.getId());

		return MessageFormat.format(COMMON_ERROR,
				user.getUserName() == null ? user.getFirstName() : user.getUserName());
	}
}
