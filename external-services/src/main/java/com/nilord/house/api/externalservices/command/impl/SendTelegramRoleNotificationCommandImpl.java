package com.nilord.house.api.externalservices.command.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nilord.house.api.externalservices.command.SendTelegramRoleNotificationCommand;
import com.nilord.house.api.externalservices.constants.ExecutionState;
import com.nilord.house.api.externalservices.constants.UserStatusEnum;
import com.nilord.house.api.externalservices.error.NiLordServiceException;
import com.nilord.house.api.externalservices.model.domain.NiLordServiceResponseEntity;
import com.nilord.house.api.externalservices.model.domain.ServiceHeaderDTO;
import com.nilord.house.api.externalservices.model.services.notification.BasicNotificationDTO;
import com.nilord.house.api.externalservices.util.message.telegram.TelegramUtils;

import lombok.extern.slf4j.Slf4j;

@Component("SendTelegramRoleNotificationCommand")
@Slf4j
public class SendTelegramRoleNotificationCommandImpl implements SendTelegramRoleNotificationCommand {

	@Autowired
	private TelegramUtils telegramUtils;

	@Override
	public NiLordServiceResponseEntity<Boolean> sendNotificationPerRole(BasicNotificationDTO request)
			throws NiLordServiceException {
		final NiLordServiceResponseEntity<Boolean> response = new NiLordServiceResponseEntity<Boolean>();

		final String finalMessage = new StringBuilder().append("NOTIFICATION <")
				.append(UserStatusEnum.getEnumFromId(request.getRoleId()).getName()).append(">: ")
				.append(request.getMessage()).toString();

		try {
			telegramUtils.sendTextMessagesPerRole(finalMessage, UserStatusEnum.getEnumFromId(request.getRoleId()));
			response.setBody(true);
			response.setHeader(new ServiceHeaderDTO(ExecutionState.SUCESS));
		} catch (NiLordServiceException e) {
			log.error("Ocurrio un error enviando notificacion");
			response.setBody(false);
			response.setHeader(new ServiceHeaderDTO(ExecutionState.FAIL));
		}

		return response;
	}

}
