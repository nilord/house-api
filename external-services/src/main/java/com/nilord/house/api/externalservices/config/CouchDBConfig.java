package com.nilord.house.api.externalservices.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

/**
 * CouchDB configuration class
 * 
 * @author nilord
 *
 */
@Configuration
public class CouchDBConfig extends AbstractMongoClientConfiguration {

	@Value("${couchdb.hostname}")
	private String COUCHDB_HOST;

	@Value("${couchdb.username}")
	private String COUCHDB_USER;

	@Value("${couchdb.password}")
	private String COUCHDB_PASSWORD;

	@Value("${couchdb.bucket}")
	private String COUCHDB_BUCKET;

	@Override
	protected String getDatabaseName() {
		return COUCHDB_BUCKET;
	}

	@Override
	@Bean
	public MongoClient mongoClient() {
		final String fullConnectionString = "mongodb://" + COUCHDB_USER + ":" + COUCHDB_PASSWORD + "@" + COUCHDB_HOST
				+ "/" + COUCHDB_BUCKET + "?authSource=admin";

		MongoClient client = MongoClients.create(fullConnectionString);
		return client;
	}
}
