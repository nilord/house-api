package com.nilord.house.api.externalservices.constants;

public enum CommandResponseType {
	SendMessage,
	SendPhoto
}
