package com.nilord.house.api.externalservices.command.impl;

import java.text.MessageFormat;
import java.util.Optional;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.nilord.house.api.externalservices.command.UpdateIpAddressCommand;
import com.nilord.house.api.externalservices.constants.BasicServiceConfigurationEnum;
import com.nilord.house.api.externalservices.constants.ExecutionState;
import com.nilord.house.api.externalservices.constants.UserStatusEnum;
import com.nilord.house.api.externalservices.error.NiLordServiceException;
import com.nilord.house.api.externalservices.model.domain.NiLordServiceResponseEntity;
import com.nilord.house.api.externalservices.model.domain.ServiceHeaderDTO;
import com.nilord.house.api.externalservices.model.domain.data.BasicServiceConfigurationDTO;
import com.nilord.house.api.externalservices.model.services.network.UpdateIpResponseDTO;
import com.nilord.house.api.externalservices.repos.BasicServiceConfigurationRepository;
import com.nilord.house.api.externalservices.util.cloudflare.CloudFlareUtils;
import com.nilord.house.api.externalservices.util.message.telegram.TelegramUtils;
import com.nilord.house.api.externalservices.util.network.NetworkUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * The class UpdateIpAddressCommandImpl
 * 
 * @author nilord
 *
 */
@Component("UpdateIpAddressCommand")
@Slf4j
public class UpdateIpAddressCommandImpl implements UpdateIpAddressCommand {

	@Autowired
	private NetworkUtils networkUtils;

	@Autowired
	private BasicServiceConfigurationRepository basicServiceConfigurationRepository;

	@Autowired
	private TelegramUtils telegramUtils;

	@Autowired
	private CloudFlareUtils cloudFlareUtils;

	@Value("${alert.notification.ipchanged.success}")
	private String IP_CHANGED_ALERT_SUCCESS;

	@Value("${alert.notification.ipchanged.failed}")
	private String IP_CHANGED_ALERT_FAILED;

	@Override
	@Scheduled(fixedRateString = "${cloudflare.check.ip.delay}")
	public NiLordServiceResponseEntity<UpdateIpResponseDTO> updateIpAddressInfo() throws NiLordServiceException {

		log.info("Inicializando comando para la actualizacion de direccion IP");

		final NiLordServiceResponseEntity<UpdateIpResponseDTO> response = new NiLordServiceResponseEntity<UpdateIpResponseDTO>();
		final String currentIp = networkUtils.getPublicIPAddress();
		final UpdateIpResponseDTO body = new UpdateIpResponseDTO();

		Optional<BasicServiceConfigurationDTO> lastIpKnown = basicServiceConfigurationRepository
				.findById(BasicServiceConfigurationEnum.LAST_IP_KNOW.getId());

		body.setHasChange(lastIpKnown.isEmpty() || !lastIpKnown.get().getValue().equals(currentIp));
		body.setNewExternalIp(currentIp);

		if (!lastIpKnown.isEmpty()) {
			final BasicServiceConfigurationDTO configInfo = lastIpKnown.get();
			body.setOldExternalIp(configInfo.getValue());
			body.setApplied(configInfo.getApplied());
		}

		if (body.getHasChange() || BooleanUtils.isNotTrue(body.getApplied())) {
			log.info("Se requiere actualizacion de informacion de IP");

			boolean sucessUpdate;
			String notificationMessage = "";

			// Update IP record in cloudflare
			try {
				sucessUpdate = cloudFlareUtils.updateDNSRecord(currentIp);
				notificationMessage = IP_CHANGED_ALERT_SUCCESS;
			} catch (NiLordServiceException e) {
				sucessUpdate = false;
				notificationMessage = IP_CHANGED_ALERT_FAILED;
				log.error("Fallo la actualizacion del IP en cloudflare {}", e);
			}

			// Send notification to telegram
			telegramUtils.sendTextMessagesPerRole(MessageFormat.format(notificationMessage, body.getOldExternalIp(),
					body.getNewExternalIp(), BooleanUtils.isNotTrue(body.getApplied())), UserStatusEnum.ADMIN);

			// Save new ip to the database
			basicServiceConfigurationRepository.save(new BasicServiceConfigurationDTO(
					BasicServiceConfigurationEnum.LAST_IP_KNOW.getId(), currentIp, sucessUpdate));
		}

		response.setBody(body);
		response.setHeader(new ServiceHeaderDTO(ExecutionState.SUCESS));

		log.info("Finalizando comando para la actualizacion de direccion IP");

		return response;
	}

}
