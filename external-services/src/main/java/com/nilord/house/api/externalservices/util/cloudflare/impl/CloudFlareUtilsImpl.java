package com.nilord.house.api.externalservices.util.cloudflare.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.nilord.house.api.externalservices.error.NiLordServiceException;
import com.nilord.house.api.externalservices.util.cloudflare.CloudFlareUtils;

import eu.roboflax.cloudflare.CloudflareAccess;
import eu.roboflax.cloudflare.CloudflareRequest;
import eu.roboflax.cloudflare.CloudflareResponse;
import eu.roboflax.cloudflare.constants.Category;
import eu.roboflax.cloudflare.objects.dns.DNSRecord;
import eu.roboflax.cloudflare.objects.zone.Zone;
import lombok.extern.slf4j.Slf4j;

@Component("CloudFlareUtils")
@Slf4j
public class CloudFlareUtilsImpl implements CloudFlareUtils {

	@Value("${cloudflare.config.token}")
	private String CF_API_KEY;

	@Value("${cloudflare.config.email}")
	private String CF_EMAIL;

	@Value("${cloudflare.config.domain}")
	private String SITE;

	private final static String HOST_ADDRESS_TYPE = "A";

	@Override
	public Boolean updateDNSRecord(String newIPAddress) throws NiLordServiceException {

		Zone siteZone = this.getZoneDomain(SITE);

		log.debug("Zone info: {}", siteZone);

		List<DNSRecord> dnsRecords = this.getSiteDNSRecords(siteZone.getId());

		log.debug("DNS records {}", dnsRecords);

		final Optional<DNSRecord> recordToUpdate = dnsRecords.stream()
				.filter(d -> HOST_ADDRESS_TYPE.equals(d.getType()) && d.getName().equals(SITE)).findFirst();

		if (recordToUpdate.isPresent()) {
			final DNSRecord record = recordToUpdate.get();
			record.setContent(newIPAddress);

			final CloudflareAccess cfAccess = new CloudflareAccess(CF_API_KEY, CF_EMAIL);

			final CloudflareResponse<DNSRecord> cfResponse = new CloudflareRequest(Category.UPDATE_DNS_RECORD, cfAccess)
					.identifiers(siteZone.getId(), record.getId()).body(new Gson().toJson(record))
					.asObjectOrObjectList(DNSRecord.class);

			if (!cfResponse.isSuccessful()) {
				log.error("Ocurrio un error en la modificacion del registro {}", record);
				throw new NiLordServiceException("Ocurrio un error en la modificacion del registro DNS en cloudflare.");
			}

			log.info("Fin al actualizar registro DNS {}", cfResponse.getJson());
		} else {
			log.error("No existe un registro tipo A para el sitio enviado que modificar {}", dnsRecords);
			throw new NiLordServiceException("No existe un registro tipo A para el sitio enviado que modificar.");
		}
		
		return true;
	}

	@Override
	public Zone getZoneDomain(final String site) throws NiLordServiceException {
		final CloudflareAccess cfAccess = new CloudflareAccess(CF_API_KEY, CF_EMAIL);
		Zone response = null;

		final CloudflareResponse<List<Zone>> cfResponse = new CloudflareRequest(Category.LIST_ZONES, cfAccess)
				.asObjectList(Zone.class);

		if (cfResponse.isSuccessful()) {
			final List<Zone> zones = cfResponse.getObject();

			Optional<Zone> fzone = zones.stream().filter(d -> site.equals(d.getName())).findFirst();

			if (fzone.isPresent()) {
				response = fzone.get();
			} else {
				throw new NiLordServiceException(
						"Ocurrio un error buscando la zona en cloudflare. No se encuentra la zona para el sitio.");
			}
		} else {
			log.error("Ocurrio un problema buscando la zona: {}", cfResponse.getJson());
			throw new NiLordServiceException("Ocurrio un error buscando la zona en cloudflare.");
		}

		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DNSRecord> getSiteDNSRecords(final String zoneID) throws NiLordServiceException {

		final CloudflareAccess cfAccess = new CloudflareAccess(CF_API_KEY, CF_EMAIL);

		final CloudflareResponse<?> cfResponse = new CloudflareRequest(Category.LIST_DNS_RECORDS, cfAccess)
				.identifiers(zoneID).asObjectOrObjectList(DNSRecord.class);

		if (!cfResponse.isSuccessful()) {
			log.error("Ocurrio un problema buscando los registros dns de la zona enviada: {}", cfResponse.getJson());
			throw new NiLordServiceException("Ocurrio un problema buscando los registros dns de la zona enviada.");
		}

		return (List<DNSRecord>) cfResponse.getObject();
	}
}
